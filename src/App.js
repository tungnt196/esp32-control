import './App.css';

import { Amplify, PubSub } from 'aws-amplify';
import { AWSIoTProvider } from '@aws-amplify/pubsub';

Amplify.configure({
  aws_cognito_region: 'ap-northeast-1', // (required) - Region where Amazon Cognito project was created
  aws_cognito_identity_pool_id: 'ap-northeast-1:c8f759f2-c6f4-4b31-b424-b8b0ce85f7dd', // (optional) - Amazon Cognito Identity Pool ID
});

// Apply plugin with configuration
Amplify.addPluggable(
  new AWSIoTProvider({
    aws_pubsub_region: 'ap-northeast-1',
    aws_pubsub_endpoint:
      'wss://a2z5hy7l5ffqhh-ats.iot.ap-northeast-1.amazonaws.com/mqtt'
  })
);

const subscribeToppics = [
  "$aws/things/ESP32/shadow/get/accepted",
  "$aws/things/ESP32/shadow/get/rejected",
  "$aws/things/ESP32/shadow/update/accepted",
  "$aws/things/ESP32/shadow/update/rejected",
  "$aws/things/ESP32/shadow/update/delta"
]

for (let i=0; i<5; i++) {
  Amplify.PubSub.subscribe(subscribeToppics[i]).subscribe({
    next: data => {
      console.log('Message received:')
      console.log(data);
    }, error: error => console.error(error),
    close: () => console.log('Done'),
  });
}

function App() {
  const controlLed = async (message) => {
    await PubSub.publish('$aws/things/ESP32/shadow/update', {
      "state": {
        "desired": {
          "fan": message
        }
      }
    });
  }

  return (
    <div className="App">
        <div style={{
          textAlign: "center"
        }}>
          <button
            onClick={() => controlLed("1")}
            style={{
              width: "80%",
              backgroundColor: "#22863a",
              margin: "20px 10%",
              color: "#fff",
              lineHeight: "50px",
              maxWidth: "200px",
            }}
          >
            On
          </button>
        </div>
        
        <div style={{
          textAlign: "center"
        }}>
          <button
            onClick={() => controlLed("0")}
            style={{
              width: "80%",
              backgroundColor: "#6f42c1",
              margin: "20px 10%",
              color: "#fff",
              lineHeight: "50px",
              maxWidth: "200px"
            }}
          >
            Off
          </button>
        </div>
    </div>
  );
}

export default App;
